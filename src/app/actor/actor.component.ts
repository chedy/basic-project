import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import {Actor} from '../model/actor';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.css']
})
export class ActorComponent implements OnInit {
  @Input() acteur:Actor;
  @Input() reference:string;
  @Output() onDeleteActor = new EventEmitter<Actor>();
  @Output() onAddFavoris = new EventEmitter<Actor>();
  constructor() {
    console.log('ActorComponent:constructor');
  }

  ngOnInit() {
    console.log("ActorComponent:init");
  }
  ngOnChanges() {
    console.log("ActorComponent: OnChange") ;
  }

  deleteActor(acteur:Actor){
    this.onDeleteActor.emit(acteur);
  }
  addFavoris(acteur:Actor){
    this.onAddFavoris.emit(acteur);
  }

}
