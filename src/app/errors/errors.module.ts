import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorsComponent } from './errors/errors.component';
import {RouterModule,Routes} from '@angular/router';
const routes:Routes=[{path:'404',component:ErrorsComponent}]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [ErrorsComponent]
})
export class ErrorsModule { }
