export class User {
  lastName:string;
  firstName:string;
  constructor(public nom :string, public email :string,public cin?:string,public role?:string) {
    this.nom=nom;
    this.email=email;
    this.cin=cin;
    this.role=role;
  };
}
