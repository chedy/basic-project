import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { FilmsComponent } from './films/films.component';
import { ActorsComponent } from './actors/actors.component';
import { ActorComponent } from './actor/actor.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {RouterModule} from '@angular/router';
import {SalleCinemaModule} from './salle-cinema/salle-cinema.module';
import {ErrorsComponent} from './errors/errors/errors.component';
import {AboutComponent} from './about/about/about.component';
import { UserFormComponent } from './user-form/user-form.component';
import {ErrorsModule} from './errors/errors.module';
import { UserReactiveFormComponent } from './user-reactive-form/user-reactive-form.component';
import { AreaComponent } from './area/area.component';
import { AreaService } from './area.service';
import { EventsComponent } from './events/events.component';
import { EventService } from './event.service';
import { HttpModule } from '@angular/http';



@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    ActorsComponent,
    ActorComponent,
    AboutComponent,
    UserFormComponent,
    UserReactiveFormComponent,
    AreaComponent,
    EventsComponent
  ],
  imports: [
    
    HttpModule,
    ErrorsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    SalleCinemaModule,
    RouterModule.forRoot([{path:'films',component:FilmsComponent},
    {path:'actors',component:ActorsComponent},
    {path:'reactive',component:UserReactiveFormComponent},
    {path:'adduser',component:UserFormComponent},
    {path:'events',component:EventsComponent},
    {path:'area',component:AreaComponent},
    {path:'**',component:ErrorsComponent}])
  ],
  providers: [AreaService,EventService],
  bootstrap: [AppComponent]

})
export class AppModule { }
