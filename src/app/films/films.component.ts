import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  listfilms : any;
  nbActions : number = 0;
  nbAutres : number = 0;

  constructor() {
    this.listfilms=[
      {'nom':'MANDY','duree':'2 heures','categorie':'action'},
      {'nom':'LOVE AFTER LOVE','duree':'3 heures','categorie':'drama'},
      {'nom':'YOU WERE NEVER REALLY HERE','duree':'2 heures','categorie':'drama'},
      {'nom':'THE RIDER','duree':'3 heures','categorie':'drama'},
      {'nom':'FIRST REFORMED','duree':'2 heures','categorie':'action'}

    ]
  }

  ngOnInit() {
  }
  calculer(){
    for (const film of this.listfilms){
      if (film.categorie === 'action'){
        this.nbActions ++;
      }else{
        this.nbAutres ++;
      }
    }

  }

}
