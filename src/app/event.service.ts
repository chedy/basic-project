import { Injectable } from '@angular/core';
import { Event } from './model/event';
import { EVENTS } from './data/events';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';

@Injectable()
export class EventService {

  constructor(private http: Http) { }

  getEvents():Observable <Event[]> {
    return of (EVENTS) ;
  }
  getEventsJson(){
    return this.http.get('../assets/events.json').map(res=>res.json());
  }

}
