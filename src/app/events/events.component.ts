import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import { Event } from '../model/event';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  eventsList :Event[] ;
    
  constructor(private e : EventService) {this.e.getEventsJson();

  }

  ngOnInit() {
     this.getEventsJson();

  }
  getEventsJson(){
    this.e.getEventsJson().subscribe(data=>{

      this.eventsList=data;
    });

  }

}
