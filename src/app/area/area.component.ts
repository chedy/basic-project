import { Component, OnInit } from '@angular/core';
import { AreaService } from '../area.service';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

  nombre = 0 ;
  surface = 0 ;

  constructor(private areaservice: AreaService) { }

  ngOnInit() {
  }
  calculercercle() {
    this.surface = (this.nombre * this.nombre * 3.14);
  }
  calculercaree() {
    this.surface = (this.nombre * this.nombre) ;
  }
  calculercercle2(nb) {
    this.surface = this.areaservice.calculercercle2(nb);

  }
  calculercaree2(nb) {
    this.surface = this.areaservice.calculercaree2(nb);

  }

}
