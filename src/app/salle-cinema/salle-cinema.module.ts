import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalleComponent } from './salle/salle.component';
import {RouterModule, Routes} from '@angular/router';
import { SalleListComponent } from './salle-list/salle-list.component';
import { SalleDetailsComponent } from './salle-details/salle-details.component';
const routes:Routes=[{path:'salles',component:SalleComponent,children:[{path:'',component:SalleListComponent},{path:':id',component:SalleDetailsComponent}]}]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[
    SalleComponent
  ],
  declarations: [SalleComponent, SalleListComponent, SalleDetailsComponent]
})
export class SalleCinemaModule { }

