import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Dashboard Admin';
  constructor(){
    console.log("AppComponent:constructor");
  }
  ngOnInit(){
    console.log("AppComponent:init");
  }
}
