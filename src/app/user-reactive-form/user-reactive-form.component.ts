import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-reactive-form',
  templateUrl: './user-reactive-form.component.html',
  styleUrls: ['./user-reactive-form.component.css']
})
export class UserReactiveFormComponent implements OnInit {
  userForm:FormGroup

  constructor(private fb:FormBuilder) { 
    this.createForm();
  }
  createForm() {
    this.userForm = this.fb.group({
      nom:  ['test' , Validators.required],
      email: ['test' , Validators.required],
      cin: '',
      })
  }

  ngOnInit() {
  }
  register() {
   console.log(this.userForm) ;
  }


}
