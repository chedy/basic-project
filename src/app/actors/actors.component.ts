import { Component, OnInit } from '@angular/core';
import {Actor} from '../model/actor';

@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.css']
})
export class ActorsComponent implements OnInit {
  ref:string="test";
  listActors:Actor[];
  listFavoris: Actor[] = [];
  showFavoris: boolean = false;
  actor:Actor=new Actor("test","test");
  constructor() {
    console.log("ActorsComponent:constructor");
  }

  ngOnInit() {
    console.log("ActorsComponent:init");
    this.listActors=[
      {'lastName':'Robert','firstName':'Julia'},
      {'lastName':'Walker','firstName':'Paul'},
      {'lastName':'Pitt','firstName':'Brad'},

    ];

  }

  delete($event){
    let index = this.listActors.indexOf($event);
    console.log(index);
    this.listActors.splice(index,1);
    console.log(this.listActors);
  }
  add($event){
    this.listFavoris.push($event);
    console.log(this.listFavoris);
  }
  getFavoris(){
    console.log(this.showFavoris);
    this.showFavoris=!this.showFavoris;
    console.log(this.showFavoris);
  }
  addActors(){
    console.log(this.actor);
    this.listActors.push(this.actor);
    console.log(this.listActors);
    this.actor=new Actor("","");
  }

}
